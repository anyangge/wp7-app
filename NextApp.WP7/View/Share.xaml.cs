﻿using System;
using System. Windows. Controls;
using System. Windows. Input;
using Microsoft. Phone. Controls;
using NextApp. WP7. AppConfig;
using NextApp. WP7. Util;

namespace NextApp. WP7. View
{
    public partial class Share : PhoneApplicationPage
    {
        #region Initialize
        public Share( )
        {
            InitializeComponent( );
			this. PrepareToOrientation( );
            this. ApplicationTitle. Text = Config. Instance. blog_title;
        }
        #endregion

        #region 进入微博分享页面
        private void share_sina_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Image img_share = sender as Image;
            if ( img_share != null )
            {
                switch ( img_share.Name )
                {
                    case "share_tencent":
                        this. NavigationService. Navigate(new Uri("/Weibo/View/TencentOAuth.xaml", UriKind. Relative));
                        break;
                    case "share_sina":
                        this. NavigationService. Navigate(new Uri("/Weibo/View/SinaOAuth.xaml", UriKind. Relative));
                        break;
                }
            }
        }
        #endregion

        #region 登入注销处理
        protected override void OnNavigatedTo(System. Windows. Navigation. NavigationEventArgs e)
        {
            Util. Tool. CheckAppBar(this);
            base. OnNavigatedTo(e);
        }
        //登入
        private void menuItem_Login_Click(object sender, EventArgs e)
        {
            Util. Tool. Login( );
        }

        //注销
        private void menuItem_Logout_Click(object sender, EventArgs e)
        {
            Util. Tool. Logout( );
            Util. Tool. CheckAppBar(this);
        }
        #endregion
    }
}