﻿using System;
using System. Windows;
using System. Windows. Controls;
using Microsoft. Phone. Controls;
using NextApp. WP7. Models;
using NextApp. WP7. Util;

namespace NextApp. WP7. View. Controls
{
    public partial class RelativePosts : UserControl
    {
        #region Initialize
        public RelativePosts( )
        {
            InitializeComponent( );
        }
        #endregion

        #region 分配相关文章数据
        public void SetDatas( RelativePost[ ] relativePosts )
        {
            if ( relativePosts. IsNotNullOrEmpty( ) )
            {
                this. list_RelativePosts. ItemsSource = relativePosts;
            }
            else
            {
                this. tblock_NoRelativePostsTip. Text = "没有相关文章";
                this. tblock_NoRelativePostsTip. Visibility = System. Windows. Visibility. Visible;
            }
        }
        #endregion

        #region 页面跳转 跳转到相关文章页面
        private void list_RelativePosts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            RelativePost p = this. list_RelativePosts. SelectedItem as RelativePost;
            if ( p != null )
            {
                this. list_RelativePosts. SelectedItem = null;
                //开始跳转
                ( Application. Current. RootVisual as PhoneApplicationFrame ). Navigate(new Uri(string. Format("/View/PostDetail.xaml?post={0}", p. id), UriKind. Relative));
            }
        }
        #endregion

        #region 将相关文章装订在桌面上
        private void menuItem_PostPinToStart_Click(object sender, RoutedEventArgs e)
        {
            MenuItem menuItem = sender as MenuItem;
            if ( menuItem != null )
            {
                RelativePost rp = menuItem. DataContext as RelativePost;
                if ( rp != null )
                {
                    Post p = new Post { id = rp. id, title = rp. title, author = rp. author };
                    Tool. CreateShellTile(p);
                }
            }
        }
        #endregion

		#region 删除文章
		private void menuItem_PostDelete_Click(object sender, RoutedEventArgs e)
		{
			if ( AppConfig.Config.Instance.IsLogin == false )
			{
				if ( MessageBox.Show("请先登录", "", MessageBoxButton.OKCancel)  == MessageBoxResult.OK)
				{
					Util. Tool. Login( );
				}
			}
		}
		#endregion
	}
}
