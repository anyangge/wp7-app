﻿using System;
using System. Windows;
using System. Windows. Controls;
using Microsoft. Phone. Tasks;
using NextApp. WP7. AppConfig;

namespace NextApp. WP7. View. Controls
{
    public partial class About : UserControl
    {
        #region Initialize
        public About( )
        {
            InitializeComponent( );
            this. Loaded += new RoutedEventHandler(About_Loaded);
        }

        private void About_Loaded(object sender, RoutedEventArgs e)
        {
            this. link_Website. Content = Config. Instance. blog_url_root;
            this. link_Support. Content = "support@nextapp.cn";
            System. Diagnostics. Debug. WriteLine(Config. Instance. about_description);
            this. tblock_Description. Text = @Config. Instance. about_description. Replace(@"\\", @"\");
        }

        #endregion

        #region 点击进入应用提供方网站
        private void link_Website_Click(object sender, RoutedEventArgs e)
        {
            WebBrowserTask wtk = new WebBrowserTask { Uri = new Uri(Config. Instance.blog_url_root, UriKind. Absolute) };
            wtk. Show( );
        }
        #endregion

        #region 给应用提供方发送邮件
        private void link_Support_Click(object sender, RoutedEventArgs e)
        {
            EmailComposeTask ect = new EmailComposeTask
            {
                To = "support@nextapp.cn",
                Subject = /*Config. Instance. about_email_title*/ "",
            };
            ect. Show( );
        }
        #endregion
    }
}
