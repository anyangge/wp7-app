﻿using System;
using System. Windows;
using System. Windows. Controls;
using System. Windows. Media;
using Microsoft. Phone. Tasks;

namespace NextApp. WP7. View. Controls
{
    public partial class ImgWithClose : UserControl
    {
        #region Initialize
        public ImgWithClose( )
        {
            InitializeComponent( );
        }

        #endregion

        #region Public Properties
        /// <summary>
        /// 图像属性
        /// </summary>
        public ImageSource Source
        {
            get { return this. img. Source; }
            set { this. img. Source = value; }
        }

        /// <summary>
        /// 图像拉伸属性
        /// </summary>
        public Stretch Stretch
        {
            get { return this. img. Stretch; }
            set { this. img. Stretch = value; }
        }

        /// <summary>
        /// 图像属性
        /// </summary>
        public PhotoResult PhotoInfo
        {
            get;
            set;
        }

        #endregion

        #region Public Events
        /// <summary>
        /// 关闭事件
        /// </summary>
        public event EventHandler Close;

        #endregion

        #region 关闭按钮内部处理事件
        private void RoundButton_Click(object sender, RoutedEventArgs e)
        {
            if ( this. Close != null )
            {
                this. Close(this, null);
            }
        }
        #endregion
    }
}
