﻿using System. Collections. Generic;
using NextApp. WP7. Models;

namespace NextApp. WP7. Util
{
    /*
     * 用于数据集合汇集后的相同项排除法
     */

    public sealed class CommentCompare : IEqualityComparer<Comment>
    {
        public bool Equals(Comment x, Comment y)
        {
            return x. id == y. id;
        }
        public int GetHashCode(Comment obj)
        {
            return base. GetHashCode( );
        }
    }

    public sealed class PostCompare : IEqualityComparer<Post>
    {
        public bool Equals(Post x, Post y)
        {
            return x. id == y. id;
        }
        public int GetHashCode(Post obj)
        {
            return base. GetHashCode( );
        }
    }
}
