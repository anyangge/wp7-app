﻿using System;
using System. IO. IsolatedStorage;
using System. Net;
using System. Xml. Linq;
using Microsoft. Phone. Info;
using Microsoft. Phone. Shell;

namespace NextApp.WP7. AppConfig
{
    public sealed class Config
    {
        #region Singleton
        private static readonly Config instance = new Config( );
        public static Config Instance { get { return instance; } }
        private Config( )
        {
            //加载 wp7 的所有设置 来自 Config.xml 文件
            this. LoadXmlSettings( );
            //加载 wp7 手机的硬件设备信息
            this. LoadPhoneDeviceInfo( );
        }
        #endregion

        #region Initialize Method
        /// <summary>
        /// 加载基本设置 来自Config.xml
        /// </summary>
        private void LoadXmlSettings( )
        {
            XElement x_settings = XElement. Load("AppConfig/Config.xml");
            if ( x_settings != null )
            {
                //主要信息获取
				string url_root = x_settings. Element("main"). Element("blog_root"). Value;
				this. blog_url_root = url_root. EndsWith("/") ? url_root : url_root + "/";
                this. blog_title = x_settings. Element("main"). Element("blog_title"). Value;
                this. blog_ApiUrl = x_settings. Element("main"). Element("blog_querystring"). Value;
                //关于我们的信息获取
                this. about_title = x_settings. Element("about"). Element("title"). Value. EnsureNotNull( );
				this. about_description = x_settings. Element("about"). Element("description"). Value. EnsureNotNull( );
            }
        }

        /// <summary>
        /// 获取手机硬件设备信息
        /// </summary>
        private void LoadPhoneDeviceInfo( )
        {
            this. PhoneDeviceName = DeviceStatus. DeviceName;
            /*
             * 暂时没办法获取到 OS version
             * 未完待续
             */
        }
        #endregion

        #region Public Method
        /// <summary>
        /// 获取 http 查询字符串
        /// </summary>
        /// <param name="querystring">查询字符串</param>
        /// <returns>包含查询字符串的全部 url</returns>
        public string GetApi(string querystring)
        {
            return (querystring. Contains("http://")||querystring.Contains("https://")) ?
            querystring : blog_url_root + querystring;
        }

        #endregion

		#region WordPress的站点根 url  站点文字 (标题) 以及 获取API的全称url
        /// <summary>
        /// 站点根url
        /// </summary>
        public string blog_url_root
        {
            get 
            {
                return PhoneApplicationService. Current. State. ContainsKey("Config_blog_url_root") ?
                    PhoneApplicationService. Current. State[ "Config_blog_url_root" ]. ToString( ) : string. Empty;
            }
            private set
            {
                PhoneApplicationService. Current. State[ "Config_blog_url_root" ] = value;
            }
        }

        /// <summary>
        /// 站点标题
        /// </summary>
        public string blog_title
        {
            get
            {
                return PhoneApplicationService. Current. State. ContainsKey("Config_blog_title") ?
                    PhoneApplicationService. Current. State[ "Config_blog_title" ]. ToString( ) : string. Empty;
            }
            private set 
            {
                PhoneApplicationService. Current. State[ "Config_blog_title" ] = value;
            }
        }

        /// <summary>
        /// 获取站点API的全称url
        /// </summary>
        public string blog_ApiUrl
        {
            get 
            {
                return PhoneApplicationService. Current. State. ContainsKey("Config_blog_ApiUrl") ?
                    PhoneApplicationService. Current. State[ "Config_blog_ApiUrl" ]. ToString( ) : string. Empty;
            }
            private set 
            {
                PhoneApplicationService. Current. State[ "Config_blog_ApiUrl" ] = value;
             }
        }

        #endregion

        #region 用户名以及密码以及 Cookie
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName
        {
            get 
            {
                return IsolatedStorageSettings. ApplicationSettings. Contains("Config_UserName") ?
                   IsolatedStorageSettings. ApplicationSettings[ "Config_UserName" ]. ToString( ) : null;
            }
            set 
            {
                IsolatedStorageSettings. ApplicationSettings[ "Config_UserName" ] = value;
                IsolatedStorageSettings. ApplicationSettings. Save( );
            }
        }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password
        {
            get
            {
                return IsolatedStorageSettings. ApplicationSettings. Contains("Config_Password") ?
                    IsolatedStorageSettings. ApplicationSettings[ "Config_Password" ]. ToString( ) : null;
            }
            set
            {
                IsolatedStorageSettings. ApplicationSettings[ "Config_Password" ] = value;
                IsolatedStorageSettings. ApplicationSettings. Save( );
            }
        }

        /// <summary>
        /// Cookie
        /// </summary>
        public string Cookie
        {
            get
            {
                return IsolatedStorageSettings. ApplicationSettings. Contains("Config_Cookie") ?
                    IsolatedStorageSettings. ApplicationSettings[ "Config_Cookie" ]. ToString( ) : null;
            }
            set
            {
                IsolatedStorageSettings. ApplicationSettings[ "Config_Cookie" ] = value;
                IsolatedStorageSettings. ApplicationSettings. Save( );
            }
        }

        /// <summary>
        /// 判断用户当前是否已经登陆
        /// </summary>
        public bool IsLogin
        {
            get 
            {
				return Cookie. IsNotNullOrWhitespace( );
            }
        }
        #endregion

        #region 轮询查找的延时
        /// <summary>
        /// 轮询查找的延时 默认30分钟
        /// </summary>
        public readonly TimeSpan TimeSpan_Polling = TimeSpan. FromMinutes(30);

        #endregion

        #region 关于我们的信息设置
        public string about_title
        {
            get 
            {
                return PhoneApplicationService. Current. State. ContainsKey("Config_about_title") ?
                    PhoneApplicationService. Current. State[ "Config_about_title" ]. ToString( ) : string. Empty;
            }
            private set
            {
                PhoneApplicationService. Current. State[ "Config_about_title" ] = value; 
            }
        }

        public string about_description
        {
            get
            {
                return PhoneApplicationService. Current. State. ContainsKey("Config_about_description") ?
                    PhoneApplicationService. Current. State[ "Config_about_description" ]. ToString( ) : string. Empty;
            }
            private set
            {
                PhoneApplicationService. Current. State[ "Config_about_description" ] = value;
            }
        }
        #endregion

        #region 手机硬件以及OS信息
        /// <summary>
        /// 手机硬件设备名称
        /// </summary>
        public string PhoneDeviceName
        {
            get 
            {
                return PhoneApplicationService. Current. State. ContainsKey("Config_PhoneDeviceName") ?
                    PhoneApplicationService. Current. State[ "Config_PhoneDeviceName" ]. ToString( ) : "wp7";
            }
            private set 
            {
                PhoneApplicationService. Current. State[ "Config_PhoneDeviceName" ] = value;
            }
        }

        /// <summary>
        /// 手机OS版本信息
        /// </summary>
        public string PhoneOSVersion
        {
            get 
            {
                return PhoneApplicationService. Current. State. ContainsKey("Config_PhoneOSVersion") ?
                    PhoneApplicationService. Current. State[ "Config_PhoneOSVersion" ]. ToString( ) : "7.10.7740.16";
            }
            private set 
            {
                PhoneApplicationService. Current. State[ "Config_PhoneOSVersion" ] = value;
            }
        }

        /// <summary>
        /// 需要传输的 http user agent
        /// </summary>
        public string Http_User_Agent
        {
            get { return string. Format("NextApp/{0}/{1} (http://www.nextapp.cn)", this. PhoneDeviceName, this. PhoneOSVersion); }
        }
        #endregion

        #region 发表评论时的固定信息 比如用户发评论的固定用户名 邮箱 网站
        public string Comment_UserName
        {
            get 
            {
                return IsolatedStorageSettings. ApplicationSettings. Contains("Config_Comment_UserName") ?
                    IsolatedStorageSettings. ApplicationSettings[ "Config_Comment_UserName" ]. ToString( ) : string. Empty;
            }
            set 
            {
                IsolatedStorageSettings. ApplicationSettings[ "Config_Comment_UserName" ] = value;
                IsolatedStorageSettings. ApplicationSettings. Save( );
            }
        }
        public string Comment_Email
        {
            get 
            {
                return IsolatedStorageSettings. ApplicationSettings. Contains("Config_Comment_Email") ?
                    IsolatedStorageSettings. ApplicationSettings[ "Config_Comment_Email" ]. ToString( ) : string. Empty;
            }
            set 
            {
                IsolatedStorageSettings. ApplicationSettings[ "Config_Comment_Email" ] = value;
                IsolatedStorageSettings. ApplicationSettings. Save( );
            }
        }
        public string Comment_Website
        {
            get 
            {
                return IsolatedStorageSettings. ApplicationSettings. Contains("Config_Comment_Website") ?
                    IsolatedStorageSettings. ApplicationSettings[ "Config_Comment_Website" ]. ToString( ) : string. Empty;
            }
            set 
            {
                IsolatedStorageSettings. ApplicationSettings[ "Config_Comment_Website" ] = value;
                IsolatedStorageSettings. ApplicationSettings. Save( );
            }
        }
        #endregion

        #region 滚动条设定阈值
		//public const double ScrollBarOffset =3.2;
		public const double ScrollBarOffset = 2;
		//public const double ScrollBarOffset = -1;

        #endregion

		#region 文章列表显示方式
		public PostListLayoutType PostListLayoutType
		{
			get
			{
				PostListLayoutType type = AppConfig. PostListLayoutType. Normal;
				IsolatedStorageSettings. ApplicationSettings. TryGetValue<PostListLayoutType>("PostListLayoutType", out type);
				return type;
			}
			set
			{
				IsolatedStorageSettings. ApplicationSettings[ "PostListLayoutType" ] = value;
				IsolatedStorageSettings. ApplicationSettings. Save( );
			}
		}

		#endregion
	}

	public enum PostListLayoutType
	{
 		Normal,
		Border1,
		Border2,
	}
}
