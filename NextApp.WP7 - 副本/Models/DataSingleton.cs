﻿using System. Collections. Generic;
using Microsoft. Phone. Shell;

namespace NextApp. WP7. Models
{
    /// <summary>
    /// 缓存类 单例模式  缓存一些重要数据
    /// </summary>
    public sealed class DataSingleton
    {
        #region Singleton
        private static readonly DataSingleton instance = new DataSingleton( );
        public static DataSingleton Instance { get { return instance; } }
		private DataSingleton( ) { }
        #endregion

        #region Public Properties
        /// <summary>
        /// 缓存当前查看的文章
        /// </summary>
        public Post postToShare
        {
            get
            {
                return PhoneApplicationService. Current. State. ContainsKey("DataSingleton_postToShare") ?
                    PhoneApplicationService. Current. State[ "DataSingleton_postToShare" ] as Post : null;
            }
            set
            {
                PhoneApplicationService. Current. State[ "DataSingleton_postToShare" ] = value;
            }
        }

        public GoToSharePageType GoToSharePageType
        {
            get 
            {
                return PhoneApplicationService. Current. State. ContainsKey("DataSingleton_GoToSharePageType") ?
                    ( GoToSharePageType )PhoneApplicationService. Current. State[ "DataSingleton_GoToSharePageType" ] : Models. GoToSharePageType. PostDetail;
            }
            set
            {
                PhoneApplicationService. Current. State[ "DataSingleton_GoToSharePageType" ] = value;
            }
        }

        /// <summary>
        /// 缓存的文章分类数据
        /// </summary>
		public Catalog[ ] catalogs
		{
			get
			{
				return PhoneApplicationService. Current. State. ContainsKey("DataSingleton_catalogs") ?
					PhoneApplicationService. Current. State[ "DataSingleton_catalogs" ] as Catalog[ ] : null;
			}
			set
			{
				PhoneApplicationService. Current. State[ "DataSingleton_catalogs" ] = value;
			}
		}

        #endregion
    }
}
