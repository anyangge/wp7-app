﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace 测试_osmobile_API.Config
{
    public class AppConfig
    {
        #region Singleton
        private static readonly AppConfig instance = new AppConfig();
        public static AppConfig Instance { get { return instance; } }
        private AppConfig() { }
        #endregion

        public const string Api_Root = "http://192.168.1.68/php/wordpress/?";
        /// <summary>
        /// 获取Api整体访问uri
        /// </summary>
        /// <param name="partial">api 查询字符串</param>
        /// <returns>整体访问uri</returns>
        public string GetApi(string querystring)
        {
            return Api_Root + querystring;
        }

    }
}
