﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;
using 测试_osmobile_API.Config;
using 测试_osmobile_API.Model;
using WP7_WebLib.HttpPost;

namespace 测试_osmobile_API.API_Test
{
    public partial class Comment_Pub : UserControl
    {
        public Comment_Pub()
        {
            InitializeComponent();
        }

        private void btn_Comment_Pub_Click(object sender, RoutedEventArgs e)
        {
            this.SendByHttpPost();
        }

        private void SendByHttpPost()
        {
            //post	整数	文章编号
            //name	字符串	评论人姓名
            //email	字符串	评论人邮箱地址
            //url	字符串	评论人网址
            //body	字符串	评论内容
            Dictionary<string, object> parameters = new Dictionary<string, object>
            {
                {"post", txt_PostID.Text.ToInt32()},
                {"name", txt_UserName.Text},
                {"email", txt_Email.Text},
                {"url", txt_Url.Text},
                {"body", txt_Body.Text},
            };
            PostClient client = new PostClient(parameters);
            client.DownloadStringCompleted += (s, e1) =>
                {
                    if (e1.Error != null)
                    {
                        System.Diagnostics.Debug.WriteLine("添加评论失败: {0}", e1.Error.Message);
                        return;
                    }
                    XElement x = XElement.Parse(e1.Result).Element("result");
                    error _error = new error
                    {
                        errorCode = x.Element("errorCode").Value.ToInt32(),
                        errorMessage = x.Element("errorMessage").Value,
                    };
                    //分析错误代码
                    System.Diagnostics.Debug.WriteLine(_error);
                    MessageBox.Show(_error.errorMessage, _error.errorCode.ToString(), MessageBoxButton.OK);
                };
            client.DownloadStringAsync(new Uri(AppConfig.Instance.GetApi("osmobile=comment&action=pub"), UriKind.Absolute));
        }
    }
}
