﻿using System;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;
using 测试_osmobile_API.Config;
using 测试_osmobile_API.Model;

namespace 测试_osmobile_API.API_Test
{
    public partial class PostDetail_Test : UserControl
    {
        public PostDetail_Test()
        {
            InitializeComponent();
        }

        private void btn_PostDetail_Test_Click(object sender, RoutedEventArgs e)
        {
            WebClient client = new WebClient();
            client.DownloadStringCompleted += (s, e1) =>
            {
                if (e1.Error != null)
                {
                    System.Diagnostics.Debug.WriteLine("获取 post-detail 出错: {0}", e1.Error.Message);
                    return;
                }
                XElement postElement = XElement.Parse(e1.Result).Element("post");
                post post = new post
                {
                    id = postElement.Element("id").Value.ToInt32(),
                    catalog = postElement.Element("catalog").Value.Split(',').ToArray().Select(c => c.ToInt32()).ToArray(),
                    title = postElement.Element("title").Value,
                    url = postElement.Element("url").Value,
                    body = postElement.Element("body").Value,
                    tags = postElement.Element("tags").Value.Split(','),
                    author = postElement.Element("author").Value,
                    pubDate = postElement.Element("pubDate").Value,
                    commentCount = postElement.Element("commentCount").Value.ToInt32(),
                    relativePosts = postElement.Element("relativePosts").Elements("relativePost").Select(
                        r => new relativePost
                        {
                            id = r.Element("id").Value.ToInt32(),
                            author = r.Element("author").Value,
                            pubDate = r.Element("pubDate").Value,
                            title = r.Element("title").Value,
                        }
                    ).ToArray(),
                };
                //测试获取整篇详细文章成功               
                System.Diagnostics.Debug.WriteLine(post);
            };
            client. DownloadStringAsync(new Uri(AppConfig. Instance. GetApi("nextapp=post&action=detail&post=4"), UriKind. Absolute));
        }
    }
}
