﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;
using 测试_osmobile_API.Config;
using 测试_osmobile_API.Model;

namespace 测试_osmobile_API.API_Test
{
    public partial class CommentList_Test : UserControl
    {
        public CommentList_Test()
        {
            InitializeComponent();
        }

        private void btn_CommentList_Test_Click(object sender, RoutedEventArgs e)
        {
            WebClient client = new WebClient();
            client.DownloadStringCompleted += (s, e1) =>
            {
                if (e1.Error != null)
                {
                    System.Diagnostics.Debug.WriteLine("获取 评论列表 出错: {0}", e1.Error.Message);
                    return;
                }
                XElement x = XElement.Parse(e1.Result);
                commentlist commentlist = new commentlist
                {
                    commentCount = x.Element("commentCount").Value.ToInt32(),
                    comments = x.Element("comments").Elements("comment").Select(
                        c => new comment
                        {
                            id = c.Element("id").Value.ToInt32(),
                            post = c.Element("post").Value.ToInt32(),
                            name = c.Element("name").Value,
                            email = c.Element("email").Value,
                            url = c.Element("url").Value,
                            body = c.Element("body").Value,
                        }
                    ).ToArray(),
                };
                //测试成功 获取了所有评论列表测试成功
                System.Diagnostics.Debug.WriteLine(commentlist);
            };
            client. DownloadStringAsync(new Uri(AppConfig. Instance. GetApi("nextapp=comment&action=list"), UriKind. Absolute));
        }
    }
}
