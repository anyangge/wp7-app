﻿using System;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;
using 测试_osmobile_API.Config;
using 测试_osmobile_API.Model;

namespace 测试_osmobile_API.API_Test
{
    public partial class PostList_Test : UserControl
    {
        public PostList_Test()
        {
            InitializeComponent();
        }

        private void btn_PostList_Click(object sender, RoutedEventArgs e)
        {
            WebClient client = new WebClient();
            client.DownloadStringCompleted += (s, e1) =>
            {
                if (e1.Error != null)
                {
                    System.Diagnostics.Debug.WriteLine("获取 post-list 出错: {0}", e1.Error.Message);
                    return;
                }
                XElement x = XElement.Parse(e1.Result);
                postlist postList = new postlist
                {
                    catalog = x.Element("catalog").Value.ToInt32(),
                    postCount = x.Element("postCount").Value.ToInt32(),
                    posts = x.Element("posts").Elements("post").Select(
                        p => new post
                        {
                            id = p.Element("id").Value.ToInt32(),
                            title = p.Element("title").Value,
                            outline = p.Element("outline").Value,
                            commentCount = p.Element("commentCount").Value.ToInt32(),
                            authorId = p.Element("authorId").Value.ToInt32(),
                            author = p.Element("author").Value,
                            catalog = p.Element("catalog").Value.Split(',').Select(c => c.ToInt32()).ToArray(),
                            pubDate = p.Element("pubDate").Value,
                        }
                    ).ToArray(),
                };
                //测试获取文章列表成功               
                System.Diagnostics.Debug.WriteLine(postList);
            };
            /*
             * 发现 fromPost 参数无意义  其他参数正常
             */
            string test = "nextapp=post&action=list&fetchCount=2";
            test = "nextapp=post&action=list";
            //test = "osmobile=post&action=list&fromPost=19";
            //test = "osmobile=post&action=list&catalog=1";
            
            client.DownloadStringAsync(new Uri(AppConfig.Instance.GetApi(test), UriKind.Absolute));
        }
    }
}
