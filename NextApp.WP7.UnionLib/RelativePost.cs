﻿using System;

namespace NextApp. WP7. Models
{
    public sealed class RelativePost
    {
        /// <summary>
        /// 文章ID
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 文章标题
        /// </summary>
        public string title { get; set; }
        /// <summary>
        /// 文章作者
        /// </summary>
        public string author { get; set; }
        /// <summary>
        /// 文章发布日期
        /// </summary>
        public string pubDate { get; set; }
        /// <summary>
        /// 文章发布日期的DateTime格式
        /// </summary>
        public DateTime pubDate_Format
        {
            get { return pubDate. ToDateTime( ); }
            set { ; }
        }
        public string pubDate_Date
        {
            get { return this. pubDate_Format. ToString("yyyy-MM-dd"); }
            set { ; }
        }
    }
}
