﻿
namespace NextApp.WP7. Models
{
    public sealed class ApiError
    {
        /// <summary>
        /// 错误代码
        /// </summary>
        public int errorCode { get; set; }
        /// <summary>
        /// 错误描述
        /// </summary>
        public string errorMessage { get; set; }
    }
}
