﻿
namespace NextApp. WP7. Models
{
    public sealed class PostList
    {
        /// <summary>
        /// 当前列表的分类 0 表示所有文章
        /// </summary>
        public int catalog { get; set; }
        /// <summary>
        /// 为指定分类下的文章数, 如果 catalog == 0 则代表总文章数目
        /// </summary>
        public int postCount { get; set; }
        /// <summary>
        /// 当前文章集合
        /// </summary>
        public Post[ ] posts { get; set; }
    }
}
