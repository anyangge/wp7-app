﻿using System;

namespace NextApp. WP7. UnionLib
{
    /// <summary>
    /// 本对象用于在后台推送用于传输
    /// </summary>
    public sealed class RenewBlogs
    {
        public int LastPostID { get; set; }

        public string Uri { get; set; }

        public TimeSpan Duration { get; set; }

        public string title { get; set; }

        public string UserAgent { get; set; }
    }
}
