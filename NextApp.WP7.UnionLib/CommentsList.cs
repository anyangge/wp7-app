﻿using System. Linq;
using System;

namespace NextApp. WP7. Models
{
    public sealed class CommentsList
    {
        /// <summary>
        /// 评论个数
        /// </summary>
        public int commentCount { get; set; }
        /// <summary>
        /// 所有评论集合
        /// </summary>
        public Comment[ ] comments { get; set; }

    }
}
