﻿using System;
using System.Windows;
using System. IO;
using System. Linq;

namespace NextApp. WP7. Models
{
    public sealed class Post
    {
        /// <summary>
        /// 文章 ID
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 文章标题
        /// </summary>
        public string title { get; set; }
        /// <summary>
        /// 文章链接地址 主要用户客户端分享到微博
        /// </summary>
        public string url { get; set; }
        /// <summary>
        /// 文章简短介绍 (概要信息)
        /// </summary>
        public string outline { get; set; }
        /// <summary>
        /// 评论个数
        /// </summary>
        public int commentCount { get; set; }
        /// <summary>
        /// 作者
        /// </summary>
        public string author { get; set; }
        /// <summary>
        /// 作者ID
        /// </summary>
        public int authorId { get; set; }
        /// <summary>
        /// 本文章所属分类的分类ID
        /// </summary>
        public int[ ] catalog { get; set; }
        /// <summary>
        /// 文章发布日期
        /// </summary>
        public string pubDate { get; set; }
		/// <summary>
		/// 图片信息
		/// </summary>
		public string img { get; set; }
		public string img1
		{
			get 
			{
				return ( img. IsNullOrWhitespace( )  ) ? "/Resource/plist1.jpg" : img;
			}
		}
		public string img2
		{
			get { return ( img. IsNullOrWhitespace( )  ) ? "/Resource/plist2.jpg" : img; }
			set { ; }
		}
		private bool isGifOrBmp
		{
			get 
			{
				string last = img. Split('.'). LastOrDefault( ). ToLower( );
				return last == "gif" || last == "bmp";
			}
		}

		public Uri imgUrl
		{
			get 
			{
				if ( img. IsNullOrWhitespace( ) )
				{
					return null;
				}
				else
					return new Uri(img, UriKind. Absolute);
			}
			set { ;}
		}
		public Visibility GifOrBmp
		{
			get 
			{
				if ( imgUrl == null )
				{
					return Visibility. Collapsed;
				}
				else
				{
					string extension = img. Split('.'). LastOrDefault( ). ToLower( );
					return ( extension == "gif" || extension == "bmp" ) ? Visibility. Visible : Visibility. Collapsed;
				}
			}
			set { ; }
		}
		public Visibility PngOrJpeg
		{
			get { return GifOrBmp == Visibility. Visible ? Visibility. Collapsed : Visibility. Visible; }
			set { ; }
		}

        /// <summary>
        /// 文章发布日期的DateTime格式
        /// </summary>
        public DateTime pubDate_Format
        {
            get { return pubDate. ToDateTime( ); }
            set { ; }
        }
        public string pubDate_Date
        {
            get { return this. pubDate_Format. ToString("yyyy-MM-dd"); }
            set { ; }
        }
		public string pubDate_Time
		{
			get { return this. pubDate_Format. ToString("yyyy-MM-dd hh:mm"); }
			set { ; }
		}

        /// <summary>
        /// 文章全部内容
        /// </summary>
        public string body { get; set; }

		/// <summary>
		/// 上一篇文章的ID
		/// </summary>
		public int previous { get; set; }

		/// <summary>
		/// 下一篇文章的ID
		/// </summary>
		public int next { get; set; }

        /// <summary>
        /// 文章标签
        /// </summary>
        public string[ ] tags { get; set; }

        /// <summary>
        /// 相关文章
        /// </summary>
        public RelativePost[ ] relativePosts { get; set; }
    }
}
