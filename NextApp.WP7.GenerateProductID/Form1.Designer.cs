﻿namespace NextApp. WP7. GenerateProductID
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System. ComponentModel. IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if ( disposing && ( components != null ) )
            {
                components. Dispose( );
            }
            base. Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent( )
        {
            this.txt_Url = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_GenerateProductID = new System.Windows.Forms.Button();
            this.txt_ProductID = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txt_Url
            // 
            this.txt_Url.Location = new System.Drawing.Point(54, 18);
            this.txt_Url.Name = "txt_Url";
            this.txt_Url.Size = new System.Drawing.Size(216, 21);
            this.txt_Url.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "网址:";
            // 
            // btn_GenerateProductID
            // 
            this.btn_GenerateProductID.Location = new System.Drawing.Point(15, 45);
            this.btn_GenerateProductID.Name = "btn_GenerateProductID";
            this.btn_GenerateProductID.Size = new System.Drawing.Size(255, 35);
            this.btn_GenerateProductID.TabIndex = 2;
            this.btn_GenerateProductID.Text = "生成 ProductID";
            this.btn_GenerateProductID.UseVisualStyleBackColor = true;
            this.btn_GenerateProductID.Click += new System.EventHandler(this.btn_GenerateProductID_Click);
            // 
            // txt_ProductID
            // 
            this.txt_ProductID.Location = new System.Drawing.Point(15, 87);
            this.txt_ProductID.Multiline = true;
            this.txt_ProductID.Name = "txt_ProductID";
            this.txt_ProductID.Size = new System.Drawing.Size(255, 163);
            this.txt_ProductID.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.txt_ProductID);
            this.Controls.Add(this.btn_GenerateProductID);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_Url);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System. Windows. Forms. TextBox txt_Url;
        private System. Windows. Forms. Label label1;
        private System. Windows. Forms. Button btn_GenerateProductID;
        private System. Windows. Forms. TextBox txt_ProductID;
    }
}

