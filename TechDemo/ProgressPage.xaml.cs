﻿using System;
using System. Collections. Generic;
using System. Linq;
using System. Net;
using System. Windows;
using System. Windows. Controls;
using System. Windows. Documents;
using System. Windows. Input;
using System. Windows. Media;
using System. Windows. Media. Animation;
using System. Windows. Shapes;
using Microsoft. Phone. Controls;
using Microsoft. Phone. Shell;
using System. Windows. Data;

namespace TechDemo
{
    public partial class ProgressPage : PhoneApplicationPage
    {
        #region ProgressIndicatorIsVisibleProperty
        public static readonly DependencyProperty ProgressIndicatorIsVisibleProperty =
            DependencyProperty. Register("ProgressIndicatorIsVisible",
            typeof(bool),
            typeof(ProgressPage),
            new PropertyMetadata(false));

        public bool ProgressIndicatorIsVisible
        {
            get { return ( bool )GetValue(ProgressIndicatorIsVisibleProperty); }
            set { SetValue(ProgressIndicatorIsVisibleProperty, value); }
        }
        #endregion

        public ProgressPage( )
        {
            //启动进度条
            InitializeComponent( );
            this. Loaded += new RoutedEventHandler(ProgressPage_Loaded);
        }

        void ProgressPage_Loaded(object sender, RoutedEventArgs e)
        {
            this. BindProgressBar( );
        }

        private void BindProgressBar( )
        {
            SystemTray. ProgressIndicator = new ProgressIndicator( );
            SystemTray. ProgressIndicator. Text = "数据传输中 使用页面";

            Binding bindingData;
            bindingData = new Binding("ProgressIndicatorIsVisible");
            bindingData. Source = this;
            BindingOperations. SetBinding(SystemTray. ProgressIndicator, ProgressIndicator. IsVisibleProperty, bindingData);
            BindingOperations. SetBinding(SystemTray. ProgressIndicator, ProgressIndicator. IsIndeterminateProperty, bindingData);
        }

        private void btn_StopProgress_Click(object sender, RoutedEventArgs e)
        {
            this. ProgressIndicatorIsVisible = false;
        }

        private void btn_StartProgress_Click(object sender, RoutedEventArgs e)
        {
            this. ProgressIndicatorIsVisible = true;
        }

        private void btn_ToControlPage_Click(object sender, RoutedEventArgs e)
        {
            this. NavigationService. Navigate(new Uri("/ProgressPageWithUserControl.xaml", UriKind. Relative));
        }

    }
}