﻿using System;
using System. Collections. Generic;
using System. Linq;
using System. Net;
using System. Windows;
using System. Windows. Controls;
using System. Windows. Documents;
using System. Windows. Input;
using System. Windows. Media;
using System. Windows. Media. Animation;
using System. Windows. Shapes;
using Microsoft. Phone. Controls;
using WP7_ControlsLib. Controls;

namespace TechDemo
{
    public partial class LargeDatas : PhoneApplicationPage
    {

        public LargeDatas( )
        {
            InitializeComponent( );
            for ( int i = 0 ; i < 30 ; i++ )
            {
                this.list. Items. Add("项目" + i);
            }
        }


        private void list_MouseMove(object sender, MouseEventArgs e)
        {
            //获取listbox的子类型ScrollViewer
            ScrollViewer scrollViewer = ControlHelper. FindChildOfType<ScrollViewer>(this.list);//ScrollViewer  scrollBar
            if ( scrollViewer == null )
            {
                throw new InvalidOperationException("error");
            }
            else
            {
                //判断当前滚动的高度是否大于或者等于scrollViewer实际可滚动高度，如果等于或者大于就证明到底了
                if ( scrollViewer. VerticalOffset >= scrollViewer. ScrollableHeight - 0.1)
                {
                    //处理listbox滚动到底的事情
                    for ( int i = 0 ; i < 10 ; i++ )
                    {
                        int k = this. list. Items. Count;
                        this. list. Items. Add("项目" + k);
                        k++;
                    }
                }
            }
        }
    }

}