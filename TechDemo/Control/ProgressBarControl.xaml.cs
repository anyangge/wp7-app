﻿using System;
using System. Collections. Generic;
using System. Linq;
using System. Net;
using System. Windows;
using System. Windows. Controls;
using System. Windows. Documents;
using System. Windows. Input;
using System. Windows. Media;
using System. Windows. Media. Animation;
using System. Windows. Shapes;
using Microsoft. Phone. Shell;
using System. Windows. Data;

namespace TechDemo. Control
{
    public partial class ProgressBarControl : UserControl
    {
        #region ProgressIndicatorIsVisibleProperty
        public static readonly DependencyProperty ProgressIndicatorIsVisibleProperty =
            DependencyProperty. Register("ProgressIndicatorIsVisible",
            typeof(bool),
            typeof(ProgressBarControl),
            new PropertyMetadata(false));

        public bool ProgressIndicatorIsVisible
        {
            get { return ( bool )GetValue(ProgressIndicatorIsVisibleProperty); }
            set { SetValue(ProgressIndicatorIsVisibleProperty, value); }
        }
        #endregion

        public ProgressBarControl( )
        {
            InitializeComponent( );
            this. Loaded += new RoutedEventHandler(ProgressBarControl_Loaded);
        }

        void ProgressBarControl_Loaded(object sender, RoutedEventArgs e)
        {
            SystemTray. ProgressIndicator = new ProgressIndicator( );
            SystemTray. ProgressIndicator. Text = "数据传输中 使用控件";

            Binding bindingData;
            bindingData = new Binding("ProgressIndicatorIsVisible");
            bindingData. Source = this;
            BindingOperations. SetBinding(SystemTray. ProgressIndicator, ProgressIndicator. IsVisibleProperty, bindingData);
            BindingOperations. SetBinding(SystemTray. ProgressIndicator, ProgressIndicator. IsIndeterminateProperty, bindingData);
        }

        private void btn_Start_Click(object sender, RoutedEventArgs e)
        {
            this. ProgressIndicatorIsVisible = true;
        }

        private void btn_Stop_Click(object sender, RoutedEventArgs e)
        {
            this. ProgressIndicatorIsVisible = false;
        }
    }
}
